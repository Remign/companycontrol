﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lab.Models;

namespace Lab.Services
{
  public class СonfidenceIntervalDistanceService
  {
    public void Execute(BaseModel baseModel)
    {
      foreach (var dateIntervalModel in baseModel.DateIntervals)
      {
        dateIntervalModel.InitializeQIntervals(baseModel.AlphaCount);
      }

      var confidenceInterval = new List<IntervalModel>();

      for (var i = 0; i < baseModel.AlphaCount; ++i)
      {
        var qInterval = new IntervalModel
        {
          From = baseModel.DateIntervals.Max(item => item.QIntervals[i].From),
          To = baseModel.DateIntervals.Max(item => item.QIntervals[i].To)
        };

        confidenceInterval.Add(qInterval);
      }

      foreach (var dateIntervalModel in baseModel.DateIntervals)
      {
        var confidenceIntervalsDistanse = new List<double>();
        var qIntervals = dateIntervalModel.QIntervals;


        for (var i = 0; i < qIntervals.Count; ++i)
        {
          var distance = Math.Abs(qIntervals[i].From - confidenceInterval[i].From) +
                         Math.Abs(qIntervals[i].To - confidenceInterval[i].To);

          confidenceIntervalsDistanse.Add(distance);
        }

        dateIntervalModel.ConfidenceIntervalDistanceSum = confidenceIntervalsDistanse.Sum();
      }

    }
  }
}