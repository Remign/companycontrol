﻿using System;
using System.Windows.Forms;
using Lab.Models;

namespace Lab
{
  public partial class Form2 : Form
  {
    private readonly BaseModel _baseModel;
    private readonly Form1 _parentForm;

    public Form2(BaseModel model, Form1 parentForm)
    {
      InitializeComponent();

      _baseModel = model;
      _parentForm = parentForm;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      var intervalModel = new DateIntervalModel
      {
        NIntervalA0 = new IntervalModel
        {
          From = Convert.ToDouble(textBox1.Text),
          To = Convert.ToDouble(textBox2.Text)
        },
        DIntervalA0 = new IntervalModel
        {
          From = Convert.ToDouble(textBox3.Text),
          To = Convert.ToDouble(textBox4.Text)
        },
        NIntervalA1 = new IntervalModel
        {
          From = Convert.ToDouble(textBox5.Text),
          To = Convert.ToDouble(textBox6.Text)
        },
        DIntervalA1 = new IntervalModel
        {
          From = Convert.ToDouble(textBox7.Text),
          To = Convert.ToDouble(textBox8.Text)
        },
        Date = dateTimePicker1.Value
      };

      if (!this.CheckIntervals(intervalModel.NIntervalA0, intervalModel.NIntervalA1) ||
          !this.CheckIntervals(intervalModel.DIntervalA0, intervalModel.DIntervalA1))
      {
        return;
      }
      _baseModel.DateIntervals.Add(intervalModel);
      _parentForm.SetValueToIntervalListBox(intervalModel);

      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private bool CheckIntervals(IntervalModel A0Interval, IntervalModel A1Interval)
    {
      return A0Interval.From < A1Interval.From && A1Interval.To < A0Interval.To;
    }
  }
}
