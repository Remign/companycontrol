﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Lab.Models;
using Lab.Services;

namespace Lab
{
  public partial class Form1 : Form
  {
    private Form _addDateIntervalForm;

    private Form _graphicForm;

    private BaseModel _baseModel;

    private readonly СonfidenceIntervalDistanceService _confidenceIntervalDistanceService;

    private readonly string _fileLocation = Environment.CurrentDirectory + @"\" + "file.bin";

    public Form1()
    {
      InitializeComponent();

      _baseModel = new BaseModel();
      _confidenceIntervalDistanceService = new СonfidenceIntervalDistanceService();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (_addDateIntervalForm == null)
      {
        _addDateIntervalForm = new Form2(_baseModel, this);
      }

      _addDateIntervalForm.ShowDialog(this);
    }

    public void SetValueToIntervalListBox(DateIntervalModel model)
    {
      this.listBox1.Items.Add(model.ToString());
      this.listBox1.Invalidate();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.SetFormValues(_baseModel);

      _confidenceIntervalDistanceService.Execute(_baseModel);

      foreach (var dateIntervalModel in _baseModel.DateIntervals)
      {
        this.listBox2.Items.Add(string.Format("{0} : {1}", dateIntervalModel.Date.Year,
          dateIntervalModel.ConfidenceIntervalDistanceSum));
      }

      this.listBox2.Invalidate();
    }

    private void SetFormValues(BaseModel baseModel)
    {
      baseModel.Description = textBox1.Text;
      baseModel.AlfaGapCount = Convert.ToInt32(textBox2.Text);
    }

    private void button4_Click(object sender, EventArgs e)
    {
      this.listBox1.Items.Clear();
      this.listBox1.Invalidate();
    }

    private void button5_Click(object sender, EventArgs e)
    {
      this.SetFormValues(_baseModel);

      using (var fileStream = new FileStream(_fileLocation, FileMode.Create))
      {
        var bf = new BinaryFormatter();

        bf.Serialize(fileStream, _baseModel);
      }
    }

    private void button6_Click(object sender, EventArgs e)
    {
      using (var fileStream = new FileStream(_fileLocation, FileMode.Open))
      {
        var bf = new BinaryFormatter();

        _baseModel = (BaseModel)bf.Deserialize(fileStream);
      }

      textBox1.Text = _baseModel.Description;
      textBox2.Text = _baseModel.AlfaGapCount.ToString();
      listBox1.Items.Clear();

      foreach (var dateIntervalModel in _baseModel.DateIntervals)
      {
        this.SetValueToIntervalListBox(dateIntervalModel);
      }

      this.Invalidate();
    }

    private void button3_Click(object sender, EventArgs e)
    {
      new Form3(_baseModel).Show();
    }
  }
}
