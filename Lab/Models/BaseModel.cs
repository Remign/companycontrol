﻿using System;
using System.Collections.Generic;

namespace Lab.Models
{
  [Serializable]
  public class BaseModel
  {
    public BaseModel()
    {
      this.DateIntervals = new List<DateIntervalModel>();
    }

    public string Description { get; set; }

    public int AlfaGapCount { get; set; }

    public int AlphaCount
    {
      get { return AlfaGapCount + 1; }
    }

    public double AlfaStep
    {
      get { return 1D / AlfaGapCount; }
    }

    public List<DateIntervalModel> DateIntervals { get; set; }
  }
}