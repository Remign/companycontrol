﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab.Models
{
  [Serializable]
  public class DateIntervalModel
  {
    public IntervalModel NIntervalA0 { get; set; }

    public IntervalModel NIntervalA1 { get; set; }

    public IntervalModel DIntervalA0 { get; set; }

    public IntervalModel DIntervalA1 { get; set; }

    public DateTime Date { get; set; }


    public double ConfidenceIntervalDistanceSum { get; set; }

    public override string ToString()
    {
      return string.Format("A0: N[{0};{1}], D[{2};{3}]; A1: N[{4};{5}], D[{6};{7}]; Date : {8}", NIntervalA0.From, NIntervalA0.To, DIntervalA0.From,
        DIntervalA0.To, NIntervalA1.From, NIntervalA1.To, DIntervalA1.From, DIntervalA1.To, Date.Year);
    }

    public List<IntervalModel> QIntervals { get; set; }

    public void InitializeQIntervals(int alphaCount)
    {
      var nIntervals = this.CreateIntervalList(alphaCount, NIntervalA0, NIntervalA1);
      var dIntervals = this.CreateIntervalList(alphaCount, DIntervalA0, DIntervalA1);

      var qIntervals = new List<IntervalModel>();

      for (var i = 0; i < alphaCount; i++)
      {
        qIntervals.Add(new IntervalModel
        {
          From = nIntervals[i].From / dIntervals[i].To,
          To = nIntervals[i].To / dIntervals[i].From
        });
      }

      this.QIntervals = qIntervals;
    }

    private List<IntervalModel> CreateIntervalList(int alphaCount, IntervalModel A0value, IntervalModel A1value)
    {
      var fromStep = (A1value.From - A0value.From) / (alphaCount - 1);
      var toStep = (A1value.To - A0value.To) / (alphaCount - 1);

      var intervalList = new List<IntervalModel>
      {
        A0value
      };

      for (var i = 1; i < alphaCount - 1; ++i)
      {
        var prevInterval = intervalList.Last();

        intervalList.Add(new IntervalModel
        {
          From = prevInterval.From + fromStep,
          To = prevInterval.To + toStep
        });
      }

      intervalList.Add(A1value);

      return intervalList;
    }
  }
}