﻿using System;

namespace Lab.Models
{
  [Serializable]
  public struct IntervalModel
  {
    public double From { get; set; }

    public double To { get; set; }

    public double Alpha { get; set; }
  }
}