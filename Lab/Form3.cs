﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Lab.Models;
using ZedGraph;

namespace Lab
{
  public partial class Form3 : Form
  {
    private readonly BaseModel _baseModel;

    public Form3(BaseModel baseModel)
    {
      _baseModel = baseModel;
      InitializeComponent();
    }

    private void Form1_Resize(object sender, EventArgs e)
    {
      SetSize();
    }

    // SetSize() is separate from Resize() so we can 
    // call it independently from the Form1_Load() method
    // This leaves a 10 px margin around the outside of the control
    // Customize this to fit your needs
    private void SetSize()
    {
      zedGraphControl.Location = new Point(10, 10);
      // Leave a small margin around the outside of the control
      zedGraphControl.Size = new Size(ClientRectangle.Width,
                              ClientRectangle.Height);
    }

    // Respond to form 'Load' event
    private void Form1_Load(object sender, EventArgs e)
    {
      // Setup the graph
      CreateGraph(zedGraphControl);
      // Size the control to fill the form with a margin
      SetSize();
    }

    // Build the Chart
    private void CreateGraph(ZedGraphControl zgc)
    {

      var list1 = new PointPairList();

      var yPoints = _baseModel.DateIntervals.OrderBy(item => item.Date.Year).Select(item => item.ConfidenceIntervalDistanceSum).ToArray();
      var xPoints = _baseModel.DateIntervals.OrderBy(item => item.Date.Year).Select(item => (double)item.Date.Year).ToArray();

      var myPane = zgc.GraphPane;

      myPane.Title.Text = "Graphics";
      myPane.XAxis.Title.Text = "Date";
      myPane.YAxis.Title.Text = "Distance";

      var myCurve = myPane.AddCurve("Confidence Interval Distance", xPoints, yPoints, Color.Red);

      zgc.AxisChange();
    }
  }
}
